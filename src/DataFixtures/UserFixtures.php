<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("en_US");
		
		$user = new User();

		$user->setNom($faker->lastName);
		$user->setPrenom($faker->firstName);
		$user->setEmail($faker->email);
		$user->setPassword($faker->password);

		$manager->persist($user);
		$manager->flush();
    }
}

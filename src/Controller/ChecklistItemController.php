<?php

namespace App\Controller;

use App\Entity\ChecklistItem;
use App\Form\ChecklistItemType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/checklist")
 * @IsGranted("ROLE_USER")
 */
class ChecklistItemController extends AbstractController
{
    /**
     * @Route("/checklist", name="checklist")
     */
    public function displayChecklist()
    {
        $repo = $this->getDoctrine()->getRepository(ChecklistItem::class);

        // show all
        $items = $repo->findAll();

        // new
        $newItem = new ChecklistItem();
        $addForm = $this->createForm(ChecklistItemType::class, $newItem, [
                "action" => $this->generateUrl("createItem")
        ])
        ->createView();
        
        // update
        $itemsAndForms = [];

        // Création des formulaires
        foreach ($items as $item) {
            $updateForm = $this->createForm(ChecklistItemType::class, $item, [
                "action" => $this->generateUrl("updateItem", [
                    "id" => $item->getId()
                ])
            ])
            ->createView();
                
            // stockage des paires
            $itemsAndForms[] = [$item, $updateForm];
        }

        return $this->render("checklist/checklist.html.twig", [
            "addForm" => $addForm,
            "itemsAndForms" => $itemsAndForms
        ]);
    }
    
    /**
     * @Route("/ajouter-item", name="createItem")
     * @IsGranted("ROLE_ADMIN")
     */
    public function createItem(Request $req)
    {
        $newItem = new ChecklistItem();

        $addForm = $this->createForm(ChecklistItemType::class, $newItem);

        $addForm->handleRequest($req);

        if ($addForm->isSubmitted() && $addForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newItem);
            $em->flush();

            $this->addFlash("success", "Nouvel item de liste ajouté (:! ");
        } elseif ($addForm->isSubmitted() && !($addForm->isValid())) {
            $this->addFlash("error", "L'entrée n'est pas valide, veuillez réessayez.");
        }

        return $this->redirectToRoute("checklist");
    }
    
    /**
     * @Route("/modifier-item/{id}", name="updateItem", requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     * @param int $id
     */
    public function updateItem(Request $req, $id)
    {
        $repo = $this->getDoctrine()->getRepository(ChecklistItem::class);
        $itemToUpdate = $repo->find($id);
        
        $updateForm = $this->createForm(ChecklistItemType::class, $itemToUpdate);
        $updateForm->handleRequest($req);

        
        if ($updateForm->isSubmitted() && $updateForm->isValid()) {
            $itemToUpdate = $updateForm->getData();

            // dd($itemToUpdate);

            $em = $this->getDoctrine()->getManager();
            $em->persist($itemToUpdate);
            $em->flush();

            $this->addFlash("notice", "Item de liste modifié ! ;)");
        } elseif ($updateForm->isSubmitted() && !($updateForm->isValid())) {
            $this->addFlash("error", "La modification n'a pu être enregistrée, veuillez réessayer.");
        }

        return $this->redirectToRoute("checklist");
    }

    /**
     * @Route("/supprimer-item/{id}", name="deleteItem", requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     * @param int $id
     */
    public function deleteItem($id)
    {
        $repo = $this->getDoctrine()->getRepository(ChecklistItem::class);
        $itemToDelete = $repo->find($id);

        if ($itemToDelete === null) {
            $this->addFlash("error", "Cet item de liste n'existe pas. Suppression impossible ou déjà effectuée ! ");

            $response = new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($itemToDelete);
        $em->flush();

        $this->addFlash("notice", "Mission accomplie : l'item a bien été supprimé de la liste ! ");

        return $this->redirectToRoute("checklist", [
            "deletedItem" => $itemToDelete
        ]);
    }
}

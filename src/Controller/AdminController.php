<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditUserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    /**
	 * @IsGranted("ROLE_ADMIN")
     * @Route("/login", name="index")
     */
    public function index(UserRepository $users): Response
    {
        return $this->render('admin/index.html.twig', [
			'controller_name' => 'AdminController',
			'users' => $users->findAll(),
        ]);
    }

	/**
	 *@IsGranted("ROLE_ADMIN")
 	* @Route("/utilisateurs/modifier/{id}", name="modifier_utilisateur")
	 */
	public function editUser(User $user, Request $request)
	{
		$form = $this->createForm(EditUserType::class, $user);
		$form->handleRequest($request);
	
		if ($form->isSubmitted() && $form->isValid()) {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($user);
			$entityManager->flush();
	
			$this->addFlash('message', 'Utilisateur modifié avec succès');
			return $this->redirectToRoute('admin_index');
		}
		
		return $this->render('admin/edituser.html.twig', [
			'userForm' => $form->createView(),
		]);
	}


}

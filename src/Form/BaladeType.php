<?php
namespace App\Form;

use App\Entity\Balade;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BaladeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('titre', TextType::class, array(
                'required' => true,
                'label' => false,
                'attr' => array('placeholder' => 'Titre',
                'style' => 'width: 200px')
            ))
            
            ->add('commune', TextType::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Commune',
                'style' => 'width: 200px')
            ))
            ->add('secteur', TextType::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Secteur',
                'style' => 'width: 200px')
            ))
            
            ->add('duree', TimeType::class, [
                'input'  => 'datetime',
                'widget' => 'choice',
            ])
            ->add('distance', TextType::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Distance',
                'style' => 'width: 200px')
            ))
            ->add('images', FileType::class, [
                'label' => "Download Image",
                'multiple' => true,
                'mapped' => false,
                'required' => false
            ])
            ->add('discription', TextareaType ::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Description :')
            ))
            ->add('lieuxDePauses', TextareaType::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Lieux de pause :')
            ))
            ->add('pointDinteret', TextareaType::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Points d’intérêt :')
            ))
            
            ->add('pointDeDepart', TextareaType::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Point de départ :')
            ))
            
            
            
            
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Balade::class,
        ]);
    }
}

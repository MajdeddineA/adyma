<?php

namespace App\Tests;

use App\Entity\User;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
	public function testCtor()
	{
		$user = new User();
		$this->assertTrue(true);
	}

	public function __construct($name = null)
	{
		parent::__construct($name);
		$this->user = new User();
	}

	public function testLastName() 	
	{
		$this->user->setNom("Test");
		$this->assertTrue(true, strlen($this->user->getNom()) >= 0);
	}
	
	public function testFirstName()
	{
		$this->user->setNom("Test");
		$this->assertTrue(true, strlen($this->user->getPrenom()) >= 0);
	}

	public function testEmail()
	{
		$this->user->setEmail("majdeddine.alhafez.simplon@gmail.com");
		$this->assertEquals($this->user->getEmail(), filter_var($this->user->getEmail(), FILTER_VALIDATE_EMAIL));
		$this->user->setEmail("majdeddine.alhafez.simplon@@gmail.com");
		$this->assertEquals(false, filter_var($this->user->getEmail(), FILTER_VALIDATE_EMAIL));
	}

    public function testId()
	{
		$this->assertTrue(true, $this->user->getId() >= 0);
	}
}

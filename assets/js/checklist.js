console.log("checklist")
//import "../styles/checklist.scss"
import "./print.js"


window.addEventListener("DOMContentLoaded", function(e)
{
	// Tableau des blocs contenant les paires (item + form) ; pour boucler
	let checklistPairs = document.getElementsByClassName('checklist-pair')

	for (let i = 0 ; i < checklistPairs.length ; ++i)
	{
		// Modif des id pour les rendre uniques (merci SymfoTwigForm....... [ Trouver une autre méthode ] )
		// let updateInputs = document.getElementsByClassName("update-input")
		// updateInputs[i].id = "updateInput"+i.toString()

		
		// =========== SWITCH item / form
		// ----------------------- item -> form
		
		let itemGroup = document.getElementById("itemGroup"+i.toString())
		let updateItemGroup = document.getElementById("updateItemGroup"+i.toString())
		let updateItemBtns = document.getElementsByClassName("update-item-btn")

		updateItemBtns[i].addEventListener("mouseup", function(e)
		{
			e.preventDefault
			
			itemGroup.classList.toggle("d-flex", false)
			itemGroup.classList.toggle("d-none", true)
			itemGroup.classList.toggle("justify-content-between", false)

			updateItemGroup.classList.toggle("d-none", false)
			updateItemGroup.classList.toggle("d-flex", true)

		})
		
		// ----------------------- form -> item
		let cancelUpdateItemBtns = document.getElementsByClassName("cancel-update-item-btn")
		
		cancelUpdateItemBtns[i].addEventListener("mouseup", function(e)
		{
			e.preventDefault
			
			itemGroup.classList.toggle("d-none", false)
			itemGroup.classList.toggle("d-flex", true)
			itemGroup.classList.toggle("justify-content-between", true)
			
			updateItemGroup.classList.toggle("d-none", true)
			updateItemGroup.classList.toggle("d-flex", false)

		})
	}
})
